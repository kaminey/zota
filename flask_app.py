#This is the entry point of the flask application
#all the view logic is registered here

from flask import Flask
import os

from views.home import home
from views.admin_signup import admin_signup
from views.admin_account_confirm import admin_account_confirm
from views.admin_login import admin_login
from views.admin_add_customer import admin_add_customer
from views.admin_dashboard import admin_dashboard
from views.customer_information import customer_information
from views.customer_individual_information import customer_individual_information
from views.compose import compose
from views.logout import logout

app = Flask(__name__)

app.register_blueprint(home)
app.register_blueprint(admin_signup)
app.register_blueprint(admin_account_confirm)
app.register_blueprint(admin_login)
app.register_blueprint(admin_add_customer)
app.register_blueprint(admin_dashboard)
app.register_blueprint(customer_information)
app.register_blueprint(customer_individual_information)
app.register_blueprint(compose)
app.register_blueprint(logout)


app.secret_key = os.urandom(24) 
if __name__ == "__main__":
	app.run(debug = True)
