#In this view customer is added by the Admin

from flask import Blueprint
from flask import render_template
from flask import session
from flask import redirect
from flask import url_for
from flask import request
from flask import flash
from flask import jsonify
import MySQLdb
from utils import application_constant
import datetime

#query strings
query = """ insert into Customer(name, email, admin_id, date_of_joining, access) values("%s", "%s",%d, "%s", %d) """

admin_add_customer = Blueprint("admin_add_customer", __name__)
@admin_add_customer.route("/admin_add_customer", methods = ["GET", "POST"])
def main():
	if not session.get("id",0): #if user is not loged in return to home page
		return render_template("zota.html")
	else:
		db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
		cursor = db.cursor()
		if request.method == "GET": #if request method is GET return the admin_add_customer page
			return render_template("admin_add_customer.html")
		else:		#if the request method is post
			name = request.form["name"]
			email = request.form["email"]
			access = 0
			admin_id = session["id"]
			date_time = datetime.datetime.now()	
			f = "%Y-%m-%d %H:%M:%S"
			date_time = date_time.strftime(f)
			try:	#try to insert data into the database
				cursor.execute(query%(name, email, admin_id, date_time, access))
				cursor.execute("commit")
			except: #if it fails then customer all ready existes as email is unique in the Customer table
				message = "Customer already exists"
				db.close()
				return render_template("admin_add_customer.html", message = message) #redirect to admin dashboard
			db.close()
			return render_template("admin_add_customer.html" , message = "Customer Information added") #if value is added to database return to admin_dashboard with a information message
			
			
			
