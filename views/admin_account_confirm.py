# Logic to generate decode the verification link and find the related user
# While signingup all the details from the user are stored in a temporary table with the status confirmed as false
# A link is generated and is sent to the email provided to check if the email address is real or fake
# User needs to click on the link to verify the account 

from flask import Blueprint
from flask import render_template
from flask import redirect
from flask import url_for
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from utils import application_constant
import itsdangerous
import MySQLdb

#all the query strings used in this file
query1 = "select confirm from Temporary_admin where id = %d"
query2 = "select name,email, password, gender from Temporary_admin where id = %d"
query3 = """ insert into Admin(name, email, password, gender) values("%s","%s","%s","%s") """
query4 = """ update Temporary_admin set confirm = "t" where id = %d """


admin_account_confirm = Blueprint("admin_account_confirm", __name__)

@admin_account_confirm.route("/admin_account_confirm/<token>")
def main(token):
	db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
	cursor = db.cursor()
	s = Serializer("abc", salt = "activate-salt")
	try:
		data = s.loads(token)
		id = data["id"]
		cursor.execute(query1%id) #check the confirm status
		confirm = cursor.fetchone()[0]
		if(confirm == "f"):    # if confirm status is false than store all of the data in Admin table
			cursor.execute(query2%id)
			result = cursor.fetchone()
			name = result[0]
			email = result[1]
			password = result[2]
			gender = result[3]
			cursor.execute(query3%(name,email, password,gender))
			cursor.execute(query4%id) #Update the confirm status as true
			cursor.execute("commit")
			db.close()
			return redirect(url_for("admin_login.main", message = "your account has been verified" ))
		else:
			return redirect(url_for("admin_login.main")) #if the confirm status is true then redirect to the login page
	except itsdangerous.SignatureExpired:
		return redirect(url_for("admin_signup.main", message = "Your verification period has expired please issue a new verification link")) # if the verification period expires then return to the signup page with above message
	cursor.close()
	db.close()
