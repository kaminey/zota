from flask import Blueprint
from flask import render_template
from flask import request
from flask import render_template
from utils import application_constant
from customer_send_mail import send_complex_message
import MySQLdb


#query string used in this file
query = "select name, email, date_of_joining from Customer where id = %d"

customer_individual_information = Blueprint("customer_individual_information", __name__)

@customer_individual_information.route("/customer/<int:customer_id>", methods = ["GET", "POST"])
def main(customer_id):
	db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
	cursor = db.cursor()
	if request.method == "GET":  #if the request method is get select all the information and return the individual information 
		cursor.execute(query%customer_id)
		result = cursor.fetchone()
		cursor.close()
		db.close()
		return render_template("customer_individual_information.html", information = result, customer_id = customer_id)
	
	else:
		cursor.execute(query%customer_id)	#if the request method is post
		result = cursor.fetchone()
		cursor.close()
		db.close()
		email = result[1]	#select email id of the customer
		subject = request.form["subject"] #subject of the email
		message = request.form["message"] #body of the email
		send_complex_message(email, subject, message) #send the message using mailgun api
		return render_template("customer_individual_information.html",information = result, customer_id = customer_id)
		
	
