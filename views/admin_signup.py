#Logic to generate the verification link
#which needs to be clicked to by user to verify their account
#Here Information of the user is first enetered into the temporary table 
#Then id related to that is queried 
#Then that id is encrypted into a key using a Serializer
#That key is appended as variable part of the admin_account_confirm url which then is decrypted back with the same Serializer

from flask import Blueprint
from flask import render_template
from flask import session
from flask import redirect
from flask import url_for
from flask import request
import MySQLdb
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from utils import application_constant
from send_mail import send_complex_message

#all the query string used in this file
query1 = """select id, confirm from Temporary_admin where email = "%s" """
query2 = """ insert into Temporary_admin(name, email, password, gender, confirm) values("%s","%s","%s","%s","%s") """
query3 = """ select id from Temporary_admin where email = "%s" """

admin_signup = Blueprint("admin_signup", __name__)
@admin_signup.route("/admin_signup", methods = ["GET", "POST"])
def main():
	if session.get("id",0):   # session object to see if user is not al ready loged in or not
		return redirect(url_for("admin_dashboard.main"))
	if request.method == "GET":  # if method is http GET
		return render_template("signup.html") 
	else:    #else if it is post
		name = request.form["name"]
		email = request.form["email"]
		gender = request.form["gender"]
		password = request.form["password"]
		db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
		cursor = db.cursor()
		cursor.execute(query1%email) #This query checks if the above email is already in the databse or not
		result = cursor.fetchone()
		if(result and result[1] != "f"): #if yes then check the confirm status. If true then redirect it to signup page with the below message
			return render_template("signup.html", message = "This Email is all ready registered")
		else:
			if(not result): # if email is not registered
				confirm = "f" # set confirm status to false
				cursor.execute(query2%(name, email, password, gender, confirm )) #Insert all the information in a tempoarary table with confirm status as false
				db.commit()
			cursor.execute(query3%email) # select the related id from the table
			id = cursor.fetchone()[0]
		db.close()
		s = Serializer("abc", salt = "activate-salt", expires_in = 86400) #create a serializer object with 86400 seconds as ttl
		token = s.dumps({ 'id': id }) #id is encrypted to token string which is a 128 bit key
		send_complex_message(email, name, token) #send it using the gunmail api
		return render_template("signup.html", message = "Check your mail")
