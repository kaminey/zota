#View to fetch all the customer entered admin
#Admin can view the list only it has entered

from flask import Blueprint
from flask import render_template
from flask import redirect
from flask import url_for
import MySQLdb
from flask import session
from utils import application_constant

#query string used in this file
query = """ select id, name, email, date_of_joining from Customer where admin_id = %d """


customer_information = Blueprint("customer_information", __name__)

@customer_information.route("/customer_information")
def main():
	if session.get("id",0): #Checks if the user is loged in or not
		db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
		cursor = db.cursor() 
		cursor.execute(query%session["id"]) #fetch all the result related to customer registered by admin
		result = cursor.fetchall()
		db.close()
		return render_template("customer_information.html", result = result, base_addr = application_constant.BASE_ADDR)
	else:		#if the Admin is not loged in redirect to login page
		return redirect(url_for("admin_login.main"))
