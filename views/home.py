from flask import Blueprint
from flask import render_template

home = Blueprint("home", __name__)

@home.route("/")
def main():
	return render_template("zota.html")
