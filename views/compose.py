#view logic to send batch mail to the recipients

from flask import Blueprint
from flask import request
from flask import session
from flask import redirect
from flask import url_for
from flask import render_template
from utils import application_constant
import MySQLdb
from customer_send_mail import send_complex_message

query = "select email from Customer where admin_id = %d"

compose = Blueprint("compose", __name__)

@compose.route("/compose", methods = ["GET","POST"])
def main():
	if not session.get("id",0):
		return redirect(url_for("admin_login.main"))
	if request.method == "GET": #if request method is get
		db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
		cursor = db.cursor()
		admin_id = session["id"]
		cursor.execute(query%admin_id) #select all the emails
		emails = cursor.fetchall()
		db.close()
		return render_template("compose.html", emails = emails)
	else:
		recipients = request.form["name"] #recipients string from the client
		subject = request.form["subject"] #subject 
		message = request.form["message"] #body of the mail
		recipients = str(recipients)	  #change unicode string to ascii
		recipients = recipients.split(",") #change comma seperated string to list of email address
		send_complex_message(recipients, subject, message) #Mailgun api to send the batch message
		return render_template("compose.html")
