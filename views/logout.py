from flask import Blueprint
from flask import redirect
from flask import url_for
from flask import session

logout = Blueprint("logout", __name__)
@logout.route("/logout")
def main():
	if session.get("id",0):
		session.pop("id", None)
		return redirect(url_for("admin_login.main"))
