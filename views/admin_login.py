from flask import Blueprint
from flask import render_template
from flask import url_for
from flask import request
from flask import session
from flask import redirect
from utils import application_constant
import MySQLdb

#query string
query = """ select id from Admin where email = "%s" and password = "%s" """

admin_login = Blueprint("admin_login", __name__)
@admin_login.route("/admin_login", methods = ["GET", "POST"])
def main():
	db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
	cursor = db.cursor()
	if session.get("id",0): #check id the user is al ready loged in or not. If yes then go to the admin dashboard
			return redirect(url_for("admin_dashboard.main"))
	if request.method == "GET":  #if the http method is get
		return render_template("admin_login.html") # login page
	else:      #if the http method is post
		email = request.form["email"]
		password = request.form["password"] #select from admin by cross checking together email and password
		cursor.execute(query%(email, password))
		result = cursor.fetchone()
		cursor.close()
		db.close()
		if(result): #if result is not null that means combination exists and user is authenticated
			session["id"] = result[0] #store the id of the admin in session object
			return redirect(url_for("admin_dashboard.main"))
		else:
			return render_template("admin_login.html", message = "Incorrect credentials")
			

