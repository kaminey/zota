create table Admin
(
	id int not null auto_increment,
	name varchar(30),
	email varchar(30),
	password varchar(300),
	gender varchar(1),
	PRIMARY KEY(id)
);

create table Temporary_admin
(
	id int not null auto_increment,
	name varchar(30),
	email varchar(30),
	password varchar(300),
	gender varchar(1),
	confirm varchar(1),
	PRIMARY KEY(id)
);

create table Customer
(
	id int not null auto_increment,
	name varchar(30),
	email varchar(30),
	admin_id int,
	date_of_joining date,
	access int,
	FOREIGN KEY( admin_id ) REFERENCES Admin(id),
	PRIMARY KEY(id),
	UNIQUE(email)

);


