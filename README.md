# README #
* Quick summary
A crm for Admins, where they can enter customer details and send mails to any of them.


### How do I get set up? ###

* Summary of set up

Download the project or clone it. Unzip the project and make change in utils folder's application_constant file.

* Configuration

In the application_constant.py file there are variables which needs to be set
First one is the domain at which it is running. Then the database server, databse server user, password and name of the database.

* Dependencies

python2.7, flask, itsdangerous and MySQLdb

* Database configuration

Make change in the utils folder. Also create the table in zota.sql file in the same database with command "source zota.sql". Here, terminal session and zota.sql needs to be in the same directory.

* How to run tests

1. Open the home page

2. Try to login without registration. It will not work

3. Sign up with your email account

4. A confirmation mail is sent to your account

5. Try to login without confirming your account. It will not work

6. Confirm your account. This time admin dashboard will open

7. Click Enter customer. Add few customer details.

8. Click cutomer details. A table will be there with all the details. For more details or send mail to any of them click on any of the row.

9. Individual Customer details will be there with the option to send mail to that customer.

10. Click on compose, here you can send mail in batches to the multiple recipients.

* Deployment instructions

Just change the file in utils folder.

Website is live at [zota](http://zota.pythonanywhere.com/)