var app = angular.module("EmailList", []); 
app.controller("myCtrl", function($scope) {
    $scope.products = [];
     $scope.firstname = "Vai";
    $scope.myFunction = function() {
        var va = $scope.products;
        var data = {
          "name":String(va),
          "subject":$scope.subject,
          "message":$scope.message
        };
        jQuery.post("{{ url_for('compose.main') }}", data);
        $scope.subject = "";
        $scope.message = "";
         $scope.errortext = "";
        $scope.message_sent = "Message sent to above Receipients";
        console.log(va);
    }
    $scope.addItem = function () {
        $scope.errortext = "";
        if (!$scope.addMe) {return;}
        if ($scope.products.indexOf($scope.addMe) == -1) {
            $scope.products.push($scope.addMe);
        } else {
             $scope.message_sent = "";
            $scope.errortext = "The item is already in list.";
        }
    }
    $scope.removeItem = function (x) {
        $scope.errortext = "";    
        $scope.products.splice(x, 1);
    }

});
