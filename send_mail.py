from flask import render_template
import requests
def send_complex_message(to, name, token):
    data = {}
    data["to"] = to
    data["from"] = "Verification link <vaibhav@videoholic.in>",
    data["subject"] = "Verification link confirmation"
    data["html"] = render_template("admin_account_confirm.html", name = name, token = token)
    return requests.post(
        "https://api.mailgun.net/v3/videoholic.in/messages",
        auth=("api", "key-33976740eeb7b47b8299f80d14311a01"),
        data= data)

